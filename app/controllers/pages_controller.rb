class PagesController < ApplicationController
  def home
    @title = "Home"
  end

  def contact
    @title = "Contact"
  end

  def about
    @title = "About course"
  end

  def outline
    @title = "Course outline"
  end

  def testimonials
    @title = "Testimonials"
  end

  def payment
    @title = "Payment"
  end
end
