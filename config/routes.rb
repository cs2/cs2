Cs2::Application.routes.draw do

  # static pages
  match "/home" => "pages#home"
  match "/contact" => "pages#contact"
  match "/about" => "pages#about"
  match "/outline" => "pages#outline"
  match "/testimonials" => "pages#testimonials"
  match "/payment" => "pages#payment"

  root :to => "pages#home"
end
